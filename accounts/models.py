from django.db import models
from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)


class User(models.Model):
    username = models.CharField(max_length=150)
    password = models.CharField(max_length=150)


# Create your models here.
